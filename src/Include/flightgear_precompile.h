// SPDX-FileComment: precompiled headee
// SPDX-FileCopyrightText: Copyright (C) 2024  James Turner james@flightgear.org
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <simgear/math/SGMath.hxx>
#include <Main/globals.hxx>
#include <FDM/fdm_shell.hxx>
#include <Navaids/NavDataCache.hxx>
#include <Navaids/positioned.hxx>
#include <Airports/airport.hxx>
#include <Scripting/NasalSys.hxx>
#include <Instrumentation/AbstractInstrument.hxx>



