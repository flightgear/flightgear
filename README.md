# FlightGear

![](https://gitlab.com/flightgear/flightgear/badges/next/pipeline.svg)
![](https://gitlab.com/flightgear/fgmeta/-/badges/release.svg?value_width=85)

## About

FlightGear is a free, open-source, GPL-licensed flight simulator available for Windows, macOS, and
Linux. It has been developed in collaboration with a large group of international volunteers for
over two decades.

Aside from being used by thousands of flight simulator enthusiasts, FlightGear has been utilized by
researchers from over **40 universities and academic institutions worldwide**, as well as
organizations like **NASA** and **DARPA**. It is also used in several
**FAA-approved flight simulators**.

## Features

- Free, open-source, and cross-platform
- Global scenery (fly anywhere on Earth)
- Spaceflight
- Complex weather modeling
- Online multiplayer and VATSIM support

## Download

Releases of FlightGear can be downloaded from our [website](https://www.flightgear.org/download).

## Documentation

> ⚠️ **Note!**
> FlightGear is undergoing a major revamp of its documentation to improve the quality of
> information, consolidate content into a single location, and provide it in a modern and easily
> accessible format built with contemporary tools.
>
> This updated documentation will include information on building FlightGear from source as well as
> contributing to the project.
>
> In the meantime, the `docs-mini/` directory, the [FlightGear Wiki](https://wiki.flightgear.org),
> and the [FlightGear Manual](https://flightgear.sourceforge.net/manual/2020.3/en/getstart-en.html)
> can be used. However, they should be cross-referenced for outdated or inaccurate information.

## License

FlightGear is licensed under the GNU GPL. For more information, see the [license](LICENSE.md).

## Contact

To get in touch, you can use the GitLab
[issue tracker](https://gitlab.com/groups/flightgear/-/issues). There is also a
[developer mailing list](https://sourceforge.net/projects/flightgear/lists/flightgear-devel).