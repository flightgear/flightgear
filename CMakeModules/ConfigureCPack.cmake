# ConfigureCPack.cmake -- Configure CPack packaging

if(EXISTS ${PROJECT_SOURCE_DIR}/.gitignore)
    file(READ .gitignore CPACK_SOURCE_IGNORE_FILES)
else()
    # clean tar-balls do not contain SCM (.git/.gitignore/...) files.
    set(CPACK_SOURCE_IGNORE_FILES
        "Makefile.am;~$;${CPACK_SOURCE_IGNORE_FILES}")
endif()

list (APPEND CPACK_SOURCE_IGNORE_FILES "${PROJECT_SOURCE_DIR}/.git;\\\\.gitignore")

set(CPACK_PACKAGE_VERSION_MAJOR ${FLIGHTGEAR_MAJOR_VERSION})
set(CPACK_PACKAGE_VERSION_MINOR ${FLIGHTGEAR_MINOR_VERSION})
set(CPACK_PACKAGE_VERSION_PATCH ${FLIGHTGEAR_PATCH_VERSION})
set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/LICENSE.md")
set(CPACK_RESOURCE_FILE_README  "${PROJECT_SOURCE_DIR}/README.md")

set(CPACK_SOURCE_GENERATOR TBZ2)
set(CPACK_SOURCE_PACKAGE_FILE_NAME "flightgear-${FLIGHTGEAR_VERSION}" CACHE INTERNAL "tarball basename")

include (CPack)

